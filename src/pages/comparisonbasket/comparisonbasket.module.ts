import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComparisonbasketPage } from './comparisonbasket';

@NgModule({
  declarations: [
    ComparisonbasketPage,
  ],
  imports: [
    IonicPageModule.forChild(ComparisonbasketPage),
  ],
})
export class ComparisonbasketPageModule {}
