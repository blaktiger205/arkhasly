import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewproductPage } from '../viewproduct/viewproduct';
import { ProductPage } from '../product/product';

/**
 * Generated class for the ArkhaslyproducerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-arkhaslyproducer',
  templateUrl: 'arkhaslyproducer.html',
})
export class ArkhaslyproducerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArkhaslyproducerPage');
  }
  gotoPage(){
    this.navCtrl.push(ProductPage )
  }
}
