import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArkhaslyproducerPage } from './arkhaslyproducer';

@NgModule({
  declarations: [
    ArkhaslyproducerPage,
  ],
  imports: [
    IonicPageModule.forChild(ArkhaslyproducerPage),
  ],
})
export class ArkhaslyproducerPageModule {}
