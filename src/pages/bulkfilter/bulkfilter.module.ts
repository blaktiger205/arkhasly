import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BulkfilterPage } from './bulkfilter';

@NgModule({
  declarations: [
    BulkfilterPage,
  ],
  imports: [
    IonicPageModule.forChild(BulkfilterPage),
  ],
})
export class BulkfilterPageModule {}
