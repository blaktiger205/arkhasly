import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataPresentationsPage } from '../data-presentations/data-presentations';

/**
 * Generated class for the BulkfilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bulkfilter',
  templateUrl: 'bulkfilter.html',
})
export class BulkfilterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BulkfilterPage');
  }
  gotoPage(){
    this.navCtrl.push(DataPresentationsPage)
  }
}
