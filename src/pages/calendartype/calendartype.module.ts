import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendartypePage } from './calendartype';

@NgModule({
  declarations: [
    CalendartypePage,
  ],
  imports: [
    IonicPageModule.forChild(CalendartypePage),
  ],
})
export class CalendartypePageModule {}
