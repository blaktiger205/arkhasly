import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoritesectionsPage } from './favoritesections';

@NgModule({
  declarations: [
    FavoritesectionsPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritesectionsPage),
  ],
})
export class FavoritesectionsPageModule {}
