import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FavoriteproductPage } from '../favoriteproduct/favoriteproduct';

/**
 * Generated class for the FavoritesectionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoritesections',
  templateUrl: 'favoritesections.html',
})
export class FavoritesectionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesectionsPage');
  }
  gotofavoriteproduct(){
    this.navCtrl.push(FavoriteproductPage)
  }
}
