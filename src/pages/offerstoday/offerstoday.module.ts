import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferstodayPage } from './offerstoday';

@NgModule({
  declarations: [
    OfferstodayPage,
  ],
  imports: [
    IonicPageModule.forChild(OfferstodayPage),
  ],
})
export class OfferstodayPageModule {}
