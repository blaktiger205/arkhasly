import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewproductPage } from '../viewproduct/viewproduct';

/**
 * Generated class for the OfferstodayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offerstoday',
  templateUrl: 'offerstoday.html',
})
export class OfferstodayPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfferstodayPage');
  }
  gotoPage(){
    this.navCtrl.push(ViewproductPage)
  }
}
