import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsectionsPage } from './productsections';

@NgModule({
  declarations: [
    ProductsectionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductsectionsPage),
  ],
})
export class ProductsectionsPageModule {}
