import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductsectionPage } from '../productsection/productsection';

/**
 * Generated class for the ProductsectionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productsections',
  templateUrl: 'productsections.html',
})
export class ProductsectionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsectionsPage');
  }
  gotoPage(){
    this.navCtrl.push(ProductsectionPage)
 }
}
