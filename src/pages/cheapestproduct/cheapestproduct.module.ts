import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheapestproductPage } from './cheapestproduct';

@NgModule({
  declarations: [
    CheapestproductPage,
  ],
  imports: [
    IonicPageModule.forChild(CheapestproductPage),
  ],
})
export class CheapestproductPageModule {}
