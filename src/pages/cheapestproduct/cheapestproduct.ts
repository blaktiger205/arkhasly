import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewproductPage } from '../viewproduct/viewproduct';
import { ProductPage } from '../product/product';

/**
 * Generated class for the CheapestproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cheapestproduct',
  templateUrl: 'cheapestproduct.html',
})
export class CheapestproductPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheapestproductPage');
  }
  gotoPage(){
  this.navCtrl.push(ProductPage)
}
}
