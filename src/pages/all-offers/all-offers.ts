import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewproductPage } from '../viewproduct/viewproduct';

/**
 * Generated class for the AllOffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-offers',
  templateUrl: 'all-offers.html',
})
export class AllOffersPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AllOffersPage');
  }
  gotoPage(){
    this.navCtrl.push(ViewproductPage)
  }
}
