import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataPresentationsPage } from './data-presentations';

@NgModule({
  declarations: [
    DataPresentationsPage,
  ],
  imports: [
    IonicPageModule.forChild(DataPresentationsPage),
  ],
})
export class DataPresentationsPageModule {}
