
import { Component,ViewChild  } from '@angular/core';

import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

/**
 * Generated class for the ViewproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewproduct',
  templateUrl: 'viewproduct.html',
})

export class ViewproductPage {
  @ViewChild('slides') slides: Slides;
  @ViewChild('slider') slider: Slides;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewproductPage');
  }
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    if(currentIndex == 3){
      this.slides.stopAutoplay();
    }
  }
  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }
  nexta() {
    this.slider.slideNext();
  }

  preva() {
    this.slider.slidePrev();
  }
}
