import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoriteproductPage } from './favoriteproduct';

@NgModule({
  declarations: [
    FavoriteproductPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoriteproductPage),
  ],
})
export class FavoriteproductPageModule {}
