import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewproductPage } from '../viewproduct/viewproduct';

/**
 * Generated class for the FavoriteproductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoriteproduct',
  templateUrl: 'favoriteproduct.html',
})
export class FavoriteproductPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoriteproductPage');
  }
  gotoPage(){
    this.navCtrl.push(ViewproductPage)
  }
 
}
