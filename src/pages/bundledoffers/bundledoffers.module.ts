import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BundledoffersPage } from './bundledoffers';

@NgModule({
  declarations: [
    BundledoffersPage,
  ],
  imports: [
    IonicPageModule.forChild(BundledoffersPage),
  ],
})
export class BundledoffersPageModule {}
