import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoritemenuPage } from './favoritemenu';

@NgModule({
  declarations: [
    FavoritemenuPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritemenuPage),
  ],
})
export class FavoritemenuPageModule {}
