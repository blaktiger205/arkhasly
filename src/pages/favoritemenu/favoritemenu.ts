import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

/**
 * Generated class for the FavoritemenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoritemenu',
  templateUrl: 'favoritemenu.html',
})
export class FavoritemenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritemenuPage');
  }
  gotoHome(){
    this.navCtrl.push(HomePage)
  }
  gotoLogin(){
    this.navCtrl.push(LoginPage)
  }
}
