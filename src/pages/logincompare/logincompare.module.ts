import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogincomparePage } from './logincompare';

@NgModule({
  declarations: [
    LogincomparePage,
  ],
  imports: [
    IonicPageModule.forChild(LogincomparePage),
  ],
})
export class LogincomparePageModule {}
