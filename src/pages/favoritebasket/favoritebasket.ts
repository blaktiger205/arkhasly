import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FavoritesectionsPage } from '../favoritesections/favoritesections';

/**
 * Generated class for the FavoritebasketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoritebasket',
  templateUrl: 'favoritebasket.html',
})
export class FavoritebasketPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritebasketPage');
  }
  gotoPage(){
    this.navCtrl.push(FavoritesectionsPage)
  }
}
