import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoritebasketPage } from './favoritebasket';

@NgModule({
  declarations: [
    FavoritebasketPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritebasketPage),
  ],
})
export class FavoritebasketPageModule {}
