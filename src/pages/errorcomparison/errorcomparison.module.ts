import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ErrorcomparisonPage } from './errorcomparison';

@NgModule({
  declarations: [
    ErrorcomparisonPage,
  ],
  imports: [
    IonicPageModule.forChild(ErrorcomparisonPage),
  ],
})
export class ErrorcomparisonPageModule {}
