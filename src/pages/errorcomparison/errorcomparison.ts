import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductsectionsPage } from '../productsections/productsections';

/**
 * Generated class for the ErrorcomparisonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-errorcomparison',
  templateUrl: 'errorcomparison.html',
})
export class ErrorcomparisonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ErrorcomparisonPage');
  }
 gotoProductsections(){
   this.navCtrl.push(ProductsectionsPage)
 }
}
