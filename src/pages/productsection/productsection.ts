import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CheapestproductPage } from '../cheapestproduct/cheapestproduct';
import { ArkhaslyproducerPage } from '../arkhaslyproducer/arkhaslyproducer';

/**
 * Generated class for the ProductsectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-productsection',
  templateUrl: 'productsection.html',
})
export class ProductsectionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsectionPage');
  }
  gotoCheapestproduct(){
    this.navCtrl.push(CheapestproductPage)
  }
  gotoArkhaslyproducer(){
    this.navCtrl.push(ArkhaslyproducerPage)
  }
}
