import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsectionPage } from './productsection';

@NgModule({
  declarations: [
    ProductsectionPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductsectionPage),
  ],
})
export class ProductsectionPageModule {}
