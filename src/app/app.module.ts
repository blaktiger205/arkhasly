import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LanguageserviceProvider } from '../providers/languageservice/languageservice';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotPage } from '../pages/forgot/forgot';
import { LocationPage } from '../pages/location/location';
import { MapPage } from '../pages/map/map';
import { BundledoffersPage } from '../pages/bundledoffers/bundledoffers';
import { ViewproductPage } from '../pages/viewproduct/viewproduct';
import { BulkfilterPage } from '../pages/bulkfilter/bulkfilter';
import { DataPresentationsPage } from '../pages/data-presentations/data-presentations';
import { AllOffersPage } from '../pages/all-offers/all-offers';
import { ProductPage } from '../pages/product/product';
import { ComparisonbasketPage } from '../pages/comparisonbasket/comparisonbasket';
import { OfferstodayPage } from '../pages/offerstoday/offerstoday';
import { FavoritemenuPage } from '../pages/favoritemenu/favoritemenu';
import { FavoritebasketPage } from '../pages/favoritebasket/favoritebasket';
import { FavoritesectionsPage } from '../pages/favoritesections/favoritesections';
import { FavoriteproductPage } from '../pages/favoriteproduct/favoriteproduct';
import { ProductsectionsPage } from '../pages/productsections/productsections';
import { ProductsectionPage } from '../pages/productsection/productsection';
import { CheapestproductPage } from '../pages/cheapestproduct/cheapestproduct';
import { ArkhaslyproducerPage } from '../pages/arkhaslyproducer/arkhaslyproducer';
import { LogincomparePage } from '../pages/logincompare/logincompare';
import { ErrorcomparisonPage } from '../pages/errorcomparison/errorcomparison';
import { SettingsPage } from '../pages/settings/settings';
import { CalendartypePage } from '../pages/calendartype/calendartype';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ForgotPage,
    LocationPage,
    MapPage,
    BundledoffersPage,
    ViewproductPage,
    BulkfilterPage,
    DataPresentationsPage,
    AllOffersPage,
    ProductPage,
    ComparisonbasketPage,
    OfferstodayPage,
    FavoritemenuPage,
    FavoritebasketPage,
    FavoritesectionsPage,
    FavoriteproductPage,
    ProductsectionsPage,
    ProductsectionPage,
    CheapestproductPage,
    ArkhaslyproducerPage,
    LogincomparePage,
    ErrorcomparisonPage,
    SettingsPage,
    CalendartypePage


  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    ForgotPage,
    LocationPage,
    MapPage,
    BundledoffersPage,
    ViewproductPage,
    BulkfilterPage,
    DataPresentationsPage,
    AllOffersPage ,
    ProductPage,
    ComparisonbasketPage,
    OfferstodayPage,
    FavoritemenuPage,
    FavoritebasketPage,
    FavoritesectionsPage,
    FavoriteproductPage,
    ProductsectionsPage,
    ProductsectionPage,
    CheapestproductPage,
    ArkhaslyproducerPage,
    LogincomparePage,
    ErrorcomparisonPage,
    SettingsPage,
    CalendartypePage
  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LanguageserviceProvider
  ]
})
export class AppModule {

}
