import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotPage } from '../pages/forgot/forgot';
import { LocationPage } from '../pages/location/location';
import { MapPage } from '../pages/map/map';
import { BundledoffersPage } from '../pages/bundledoffers/bundledoffers';
import { ViewproductPage } from '../pages/viewproduct/viewproduct';
import { BulkfilterPage } from '../pages/bulkfilter/bulkfilter';
import { DataPresentationsPage } from '../pages/data-presentations/data-presentations';
import { AllOffersPage } from '../pages/all-offers/all-offers';
import { ProductPage } from '../pages/product/product';
import { ComparisonbasketPage } from '../pages/comparisonbasket/comparisonbasket';
import { OfferstodayPage } from '../pages/offerstoday/offerstoday';
import { FavoritemenuPage } from '../pages/favoritemenu/favoritemenu';
import { FavoritebasketPage } from '../pages/favoritebasket/favoritebasket';
import { FavoritesectionsPage } from '../pages/favoritesections/favoritesections';
import { FavoriteproductPage } from '../pages/favoriteproduct/favoriteproduct';
import { ProductsectionsPage } from '../pages/productsections/productsections';
import { ProductsectionPage } from '../pages/productsection/productsection';
import { CheapestproductPage } from '../pages/cheapestproduct/cheapestproduct';
import { ArkhaslyproducerPage } from '../pages/arkhaslyproducer/arkhaslyproducer';
import { LogincomparePage } from '../pages/logincompare/logincompare';
import { ErrorcomparisonPage } from '../pages/errorcomparison/errorcomparison';
import { SettingsPage } from '../pages/settings/settings';
import { CalendartypePage } from '../pages/calendartype/calendartype';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any =HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

}
